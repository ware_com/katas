### Objective

Using a modern framework of your choice, your assignment is to implement a new Rock homepage and get it looking as close to the design as possible.

### Brief

The design team at Rock has provided you with designs for a new homepage! Your task is to build out the project to the designs inside the `/design` folder. You can use any tools you like to help you complete the challenge. So if you've got something you'd like to practice, feel free to give it a go.

### Tasks

-   Implement assignment using:
    -   Framework: **Any modern frontend framework of your choice**
-   Your users should be able to:
    -   View the page based on a device screen size most appropriate for a desktop
    -   Utilise the correct fonts within in the `/fonts` folder
    -   Display a list dropdown menu when the `Services` menu item is clicked
    -   Display a image based menu when the `Insights` menu item is clicked
    -   Implement a carousel for the `Application Services`
-   You will find all the required assets in the `/images` folder.

### Evaluation Criteria

-   **SOLID** coding principles utilised
-   Show us your work through your commit history
-   We're looking for you to produce working code, with enough room to demonstrate how to structure components in a small program
-   Completeness: did you complete the features?
-   Correctness: does the functionality act in sensible, thought-out ways?
-   Maintainability: is it written in a clean, maintainable way?
-   Testing: is the system adequately tested?

### Deliverables

Make sure to include all source code in the repository. 

### Submission

Please organize, design, test and document your code as if it were going into production - then push your changes to the master branch. After you have pushed your code, you may submit the assignment on the assignment page.

All the best and happy coding,

The Rock Team