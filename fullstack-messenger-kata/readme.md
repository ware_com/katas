# 🚀 Full Stack Assessment

## 📝 Description

This assessment is designed to showcase your fullstack skills using **React.js** and **Laravel**. It involves creating API endpoints on the backend server, setting up WebSocket events, and building a UI on the frontend.

## 📋 Prerequisites

The project is set up with two directories:
- `frontend`: Contains the React code.
- `backend`: Contains the Laravel project.

## ⚙️ Backend Installation

1. Navigate to the `backend` directory.
2. Run `composer install`.
3. Run `php artisan migrate`.
4. Copy the `.env.example` file to `.env`.
5. Run `php artisan key:generate`.
6. Run `php artisan queue:listen`.
7. Run `php artisan reverb:start`.
8. Run `php artisan serve`.

## 🌐 Frontend Installation

1. Navigate to the `frontend` directory.
2. Run `npm install`.
3. Run `npm start`.

## 🛠️ Backend Tasks

- [ ] Create a model, migration, and controller for the messages table.
- [ ] Create/modify an API endpoint to fetch messages.
- [ ] Create an API endpoint to send messages.
- [ ] Create a WebSocket event to allow for real-time messaging.
- [ ] Write a feature test for **one** of the endpoints.

## 💻 Frontend Tasks

- [ ] Implement a chat UI using [this Figma design](https://www.figma.com/design/hlJ6P8WmqgCeGiFZwKziMF/FullStack-Messenger-Kata?node-id=0-1&t=p8LJvpjxd7f0vgNr-1) utilizing **Tailwind** for styling.
- [ ] Implement hooks and API calls to fetch and send messages.
- [ ] Implement a WebSocket connection to receive messages in real-time and update the UI with changes.
- [ ] Utilize the existing WebSocket connection to show active status in real-time.

Good luck and happy coding! 🚀👨‍💻👩‍💻
