import {
    BrowserRouter, Navigate, Outlet,
    Route,
    Routes,
} from "react-router-dom";

import {Login} from '../pages/login';
import {Register} from "../pages/register";
import {Messages} from "../pages/messages";
import {getToken} from "../../utils/cookie";

const ProtectedRoute = ({ children }: { children: JSX.Element }) => {
    const token = getToken();
    return token ? children : <Navigate to="/login" replace />;
};

export const Index = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route
                    path="/messages"
                    element={
                        <ProtectedRoute>
                            <Messages />
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/"
                    element={
                        <ProtectedRoute>
                            <Navigate to="/messages" replace />
                        </ProtectedRoute>
                    }
                />
                <Route path="*" element={<Navigate to="/login" replace />} />
            </Routes>
        </BrowserRouter>
    );
};
