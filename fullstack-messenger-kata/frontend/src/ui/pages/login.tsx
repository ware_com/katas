import {deleteCookie, setCookie} from "../../utils/cookie";
import {useNavigate} from "react-router-dom";
import {useState} from "react";
import {RegisterResponse} from "../../types/api";
import {useAuthenticateCredentials} from "../../hooks/api";

export const Login = () => {
    const navigate = useNavigate();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const successFn = (data: RegisterResponse) => {
        deleteCookie();
        setCookie("user", JSON.stringify(data));
        navigate("/messages");
    }

    const [login] = useAuthenticateCredentials(
        {
            email: email,
            password: password,
        },
        {
            errorFn: () => alert("Incorrect email or password"),
            successFn,
        }
    );



    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        login();
    }

    return (
        <div className="flex min-h-[840px] items-center flex-col bg-white">
            <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
                <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                    <svg xmlns="http://www.w3.org/2000/svg" width="139" height="32" viewBox="0 0 140 32" fill="none">
                        <path
                            d="M8.9096 31.9945C8.21432 32.0245 7.54928 31.9345 6.88424 31.6645C4.40544 30.7044 3.22649 27.8841 2.50099 25.3039C0.294252 17.4433 -0.189414 9.01255 1.56388 1.03188C1.65457 0.641843 1.77549 0.221808 2.10801 0.0717955C2.34984 -0.0482146 2.62191 0.0117886 2.86374 0.0417912C3.80085 0.191804 4.70773 0.341816 5.64484 0.491829C6.12851 0.581837 6.61218 0.671846 7.00516 0.971871C7.91204 1.66193 7.73066 3.07205 7.51905 4.18215C6.91447 7.42242 6.67263 10.9627 6.85401 14.233C6.9447 15.9131 7.06561 17.4733 7.36791 19.1234C7.63997 20.5935 8.00272 22.1537 8.57708 23.5338C8.66777 23.7438 8.75846 23.9538 8.97006 24.0738C9.36304 24.3138 9.84671 23.9538 10.149 23.5938C12.1139 21.4036 13.3835 18.6734 15.0764 16.2732C16.7692 13.873 19.0969 11.6528 21.9989 11.2927C23.9336 11.0527 25.8985 11.6828 27.5006 12.7929C29.1028 13.933 30.3422 15.5231 31.3095 17.2332C32.6094 19.5734 33.6069 22.3937 35.9648 23.5638C38.1413 24.6439 40.9526 23.8338 42.6153 22.0337C44.2779 20.2335 44.9731 17.6533 44.9429 15.1631C44.9429 14.233 44.9127 13.1229 45.6684 12.5829C46.0312 12.3128 46.5148 12.2828 46.9683 12.2228C47.7845 12.1628 48.6007 12.1028 49.4168 12.0128C49.9005 11.9828 50.4749 11.9528 50.8376 12.2828C51.1701 12.5829 51.2608 13.0629 51.3213 13.5129C51.9561 19.0034 49.9005 24.8839 45.5475 28.1842C41.1945 31.4544 34.5743 31.5445 30.5538 27.8541C27.9238 25.4539 26.6542 21.9136 24.5684 19.0034C24.1754 18.4634 23.6917 17.9233 23.0569 17.8633C22.15 17.7433 21.4245 18.6434 20.9106 19.4234C19.5503 21.4936 18.19 23.5338 16.7994 25.604C15.6205 27.3741 14.4113 29.1743 12.7487 30.4344C11.6605 31.3344 10.2699 31.9045 8.9096 31.9945Z"
                            fill="#175CFF"></path>
                        <path
                            d="M131.127 30.4827C135.268 30.4827 138.049 28.3225 139.168 24.8722H135.449C134.754 26.4623 133.243 27.4224 131.127 27.4224C127.922 27.4224 125.897 25.4422 125.746 22.202H139.379C139.44 21.4519 139.5 20.9718 139.5 20.4918C139.5 15.0313 136.205 11.5811 130.854 11.5811C125.746 11.5811 122.148 15.1814 122.148 20.9418C122.148 26.7323 125.715 30.4827 131.127 30.4827ZM125.715 19.3817C125.867 16.5015 127.801 14.4913 130.824 14.4913C133.998 14.4913 135.782 16.4715 135.782 19.2017V19.3817H125.715Z"
                            fill="#0A083B"></path>
                        <path
                            d="M110.54 29.9426H114.168V18.9317C114.168 16.5315 115.105 15.2714 117.342 15.2714H121.12V12.1211H117.674C115.558 12.1211 114.198 13.2312 113.744 15.0013V12.1211H110.54V29.9426Z"
                            fill="#0A083B"></path>
                        <path
                            d="M102.952 29.9126H106.55V12.0911H103.345V14.5813C102.136 12.6911 99.8993 11.5811 97.088 11.5811C91.9792 11.5811 88.4424 15.3614 88.4424 21.0018C88.4424 26.6723 91.949 30.4526 97.088 30.4526C99.7179 30.4526 101.834 29.4926 102.952 27.6924V29.9126ZM92.1908 21.0018C92.1908 17.3415 94.3976 14.8513 97.6623 14.8513C100.927 14.8513 103.043 17.3415 103.043 21.0018C103.043 24.7222 100.927 27.2124 97.6623 27.2124C94.3976 27.2124 92.1908 24.7222 92.1908 21.0018Z"
                            fill="#0A083B"></path>
                        <path
                            d="M65.6198 30.1643H69.8821L73.9329 16.4231L77.9231 30.1643H82.1855L87.7779 12.3428H83.9992L79.9787 26.1739L75.9884 12.3428H71.847L67.8265 26.1739L63.8363 12.3428H60.0576L65.6198 30.1643Z"
                            fill="#0A083B"></path>
                    </svg>
                    <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
                        Sign in to your account
                    </h2>
                </div>

                <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
                    <form onSubmit={handleSubmit} className="space-y-6">
                        <div>
                            <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">
                                Email address
                            </label>
                            <div className="mt-2">
                                <input
                                    onChange={(e) => setEmail(e.target.value)}
                                    id="email"
                                    name="email"
                                    type="email"
                                    required
                                    autoComplete="email"
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                            </div>
                        </div>

                        <div>
                            <div className="flex items-center justify-between">
                                <label htmlFor="password" className="block text-sm font-medium leading-6 text-gray-900">
                                    Password
                                </label>
                            </div>
                            <div className="mt-2">
                                <input
                                    onChange={(e) => setPassword(e.target.value)}
                                    id="password"
                                    name="password"
                                    type="password"
                                    required
                                    autoComplete="current-password"
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                />
                            </div>
                        </div>

                        <div>
                            <button
                                type="submit"
                                className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                            >
                                Sign in
                            </button>

                            <p className="mt-8 text-center text-sm text-gray-500 flex flex-row gap-1 items-center justify-center">
                                Not a member?
                                <a href={"/register"} className="font-semibold text-blue-600 hover:text-blue-500">
                                    Register
                                </a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
};