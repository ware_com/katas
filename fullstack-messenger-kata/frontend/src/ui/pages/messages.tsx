import { deleteCookie, getTrimmedToken } from "../../utils/cookie";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import Pusher from "pusher-js";
import { useGetUsers } from "../../hooks/api";

export type User = {
    id: number;
    name: string;
    email: string;
    email_verified_at: string | null;
    created_at: string;
    updated_at: string;
}

type UserWsResponse = {
    user: User;
    activeStatus: string;
}

export const Messages = () => {
    const user = getTrimmedToken();
    const users = useGetUsers() as User[];  // Ensure users is an array of User
    const navigate = useNavigate();
    const [activeUsers, setActiveUsers] = useState<UserWsResponse[]>([]);

    console.log(users);

    const signOut = () => {
        deleteCookie();
        navigate("/login");
    }

    useEffect(() => {
        const pusher = new Pusher("ed5zsi5ebpdmawcqbwva", {
            wsHost: "localhost",
            wsPort: 8080,
            forceTLS: false,
        });

        const channel = pusher.subscribe('user-status');
        channel.bind('active-status', (data: UserWsResponse) => {
            setActiveUsers(prevActiveUsers => [...prevActiveUsers, data]);
        });

        return () => {
            channel.unbind_all();
            channel.unsubscribe();
        };
    }, []);

    return (
        <>
            {user}
            <button
                className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                onClick={signOut}
            >
                Sign out
            </button>
            <div>Active Status Updates</div>
            {activeUsers.map((activeUser, index) => (
                <div key={index}>
                    {JSON.stringify(activeUser)}
                    <br />
                </div>
            ))}
            <div>All users</div>
            {users.map((user, index) => (
                <div key={index}>
                    {JSON.stringify(user)}
                    <br />
                </div>
            ))}
        </>
    );
};
