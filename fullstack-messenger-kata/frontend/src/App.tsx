import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { Index } from "./ui/routes";
import Pusher from "pusher-js";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

export const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <Index />
    </QueryClientProvider>
  );
};
