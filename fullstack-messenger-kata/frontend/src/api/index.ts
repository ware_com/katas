import {
    AuthParams, RegisterParams,
} from "../types/api";
import {createApiRequest} from "./helper";

export const postAuthenticateCredentials = async ({
                                                      email,
                                                      password,
                                                  }: AuthParams): Promise<any> => {
    return createApiRequest("api/login")({
        method: "POST",
        data: {email, password},
    }) as Promise<any>;
};

export const registerUser = async ({
                                       name,
                                       email,
                                       password,
                                   }: RegisterParams): Promise<any> => {
    return createApiRequest("api/register")({
        method: "POST",
        data: {name, email, password},
    }) as Promise<any>;
};

export const getUsers = async(): Promise<any> => {
    return createApiRequest("api/users")({
        method: "GET",
    }) as Promise<any>;
}
