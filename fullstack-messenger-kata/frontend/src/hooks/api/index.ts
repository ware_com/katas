import {
  UseMutateFunction,
  useMutation, useQuery,
} from "@tanstack/react-query";

import {
  getUsers,
  postAuthenticateCredentials, registerUser,
} from "../../api";

import {
  LoginResponse,
  QueryCallbackParams, RegisterParams, RegisterResponse,
} from "../../types/api";
import {User} from "../../ui/pages/messages";

type AuthParams = {
  email: string;
  password: string;
};

export const useAuthenticateCredentials = (
    { email, password }: AuthParams,
    { successFn, errorFn }: QueryCallbackParams
): [
  UseMutateFunction<LoginResponse, any, void, unknown>,
  boolean,
      Error | string | null,
] => {
  const onError = (error: Error | string) => {
    if (!!errorFn) errorFn(`${error}`);
  };

  const mutationFn = async () => {
    return await postAuthenticateCredentials({ email, password });
  };

  const onSuccess = (data: LoginResponse) => {
    if (!!successFn) successFn(data);
  };

  const {
    isLoading: isAuthenticating,
    mutate,
    error,
  } = useMutation({
    mutationKey: ["useAuthenticateCredentials"],
    mutationFn,
    onSuccess,
    onError,
  });

  return [mutate, isAuthenticating, error];
};


export const useRegisterUser = (
    { name, email, password }: RegisterParams,
    { successFn, errorFn }: QueryCallbackParams
): [
  UseMutateFunction<RegisterResponse, any, void, unknown>,
  boolean,
      Error | string | null,
] => {
  const onError = (error: Error | string) => {
    if (!!errorFn) errorFn(`${error}`);
  };

  const mutationFn = async () => {
    return await registerUser({name, email, password });
  };

  const onSuccess = (data: RegisterResponse) => {
    if (!!successFn) successFn(data);
  };

  const {
    isLoading: isAuthenticating,
    mutate,
    error,
  } = useMutation({
    mutationKey: ["useRegisterUser"],
    mutationFn,
    onSuccess,
    onError,
  });

  return [mutate, isAuthenticating, error];
};


export const useGetUsers = (): User[] => {
  const queryFn = () => getUsers();

  const { data } = useQuery({
    queryKey: [`useGetUsers`],
    queryFn,
  });

  return [data];
}