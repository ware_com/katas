

export const setCookie = (
    name: string,
    value: any,
    expires?: number
) => {
    let cookie = name + "=" + encodeURIComponent(value);

    if (expires) {
        const expirationDate = new Date();
        expirationDate.setDate(expirationDate.getDate() + expires);
        cookie += "; expires=" + expirationDate.toUTCString();
    }

    cookie += "; path=/";

    document.cookie = cookie;
};

//Get token without quotes
export const getTrimmedToken = () => {
    const cookies = document.cookie.split(";");

    // Find the "token" cookie if it exists
    const tokenCookie = cookies.find((cookie) =>
        cookie.trim().startsWith("user=")
    );

    if (tokenCookie) {
        const [, tokenValue] = tokenCookie.trim().split("=");
        return decodeURIComponent(tokenValue);
    } else {
        return null;
    }
};

//Get token with quotes
export const getToken = () => {
    if (!getTrimmedToken()) return null;
    return `"${getTrimmedToken()}"`;
};

export const deleteCookie = () => {
    document.cookie = "user=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
};
