<?php

use App\Providers\RouteServiceProvider;
use Illuminate\Broadcasting\BroadcastServiceProvider;

return [
    App\Providers\AppServiceProvider::class,
    RouteServiceProvider::class,
    BroadcastServiceProvider::class,
];
