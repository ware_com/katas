<?php

namespace App\Repository;

use App\Models\User;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;

readonly class UserRepository
{
    public function __construct(private User $model)
    {
    }

    /**
     * @throws HttpResponseException
     */
    public function register(array $data): User
    {
        try {
            return $this->model->query()->create($data);
        } catch (Exception $e) {
            throw new HttpResponseException(response()->json(['error' => $e->getMessage()]));
        }
    }

    public function index(): array
    {
        return $this->model->query()->get()->toArray();
    }
}