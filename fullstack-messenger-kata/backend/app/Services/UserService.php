<?php

namespace App\Services;

use App\Events\ActiveStatusEvent;
use App\Models\User;
use App\Repository\UserRepository;
use App\UserChatStatus;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

readonly class UserService
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    /**
     * @throws HttpResponseException
     */
    public function register(array $data): User
    {
        return $this->userRepository->register($data);
    }

    /**
     * @throws HttpResponseException
     */
    public function login(array $data): Authenticatable
    {
        return Auth::attempt(['email' => strtolower($data['email']), 'password' => $data['password']])
            ? $this->authUserAndEvent()
            : throw new HttpResponseException(response()->json(['error' => 'Unauthorized'], 401));
    }

    private function authUserAndEvent(): ?Authenticatable
    {
        $user = Auth::user();
        ActiveStatusEvent::dispatch($user, UserChatStatus::ACTIVE);
        return $user;
    }

    public function index(): array
    {
        return $this->userRepository->index();
    }
}