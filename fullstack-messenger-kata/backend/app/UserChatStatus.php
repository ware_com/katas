<?php

namespace App;

enum UserChatStatus: string
{
    case ACTIVE = 'active';
    case INACTIVE = 'inactive';
}
