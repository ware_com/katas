<?php

namespace app\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\JsonResponse;

readonly class UserController
{
    public function __construct(private UserService $service){}

    public function index(): JsonResponse
    {
        return response()->json($this->service->index());
    }
}