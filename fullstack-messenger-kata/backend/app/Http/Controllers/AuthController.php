<?php

namespace app\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;

readonly class AuthController
{
    public function __construct(private readonly UserService $userService)
    {
    }


    public function register(UserRegisterRequest $request): JsonResponse
    {
        return response()->json($this->userService->register($request->validated()));
    }

    public function login(LoginRequest $request): JsonResponse
    {
        return response()->json($this->userService->login($request->validated()));
    }
}