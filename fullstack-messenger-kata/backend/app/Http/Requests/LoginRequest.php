<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|',
            'password' => 'required|string',
        ];
    }

    protected function failedValidation(Validator $validator): void
    {
        $this->throwErrors($validator->errors()->toArray());
    }

    private function throwErrors(array $errors): void
    {
        $response = [
            'status' => 'error',
            'errors' => $errors,
        ];

        throw new HttpResponseException(response()->json($response, 400));
    }
}
