### Objective

Using TypeScript, React, and Tailwind CSS, your assignment is to implement a responsive signup form and get it looking as close to the design as possible. This assignment will evaluate your proficiency in TypeScript, React, Tailwind CSS, State Management, Component Separation, and UI Design.

### Brief

The design team at Ware has provided you with designs for a new signup form! Your task is to build out the project to the designs inside the `/design` folder. You will find both a mobile and a desktop version of the design to work to. You can use any tools you like to help you complete the challenge. So if you've got something you'd like to practice, feel free to give it a go.

### Tasks

-   Implement assignment using:
    -   Language: **TypeScript**
    -   Framework: **React**
    -   CSS Framework: **Tailwind CSS**
-   Your users should be able to:
    -   View the optimal layout for the site depending on their device's screen size
    -   See hover states for all interactive elements on the page
    -   Receive an error message when the `form` is submitted if:
        -   Any `input` field is empty. The message for this error should say _"[Field Name] cannot be empty"_
        -   The email address is not formatted correctly (i.e. a correct email address should have this structure: `name@host.tld`). The message for this error should say _"Looks like this is not an email"_
-   You will find all the required assets in the `/images` folder. The assets are already optimized.
-   There is also a `style-guide.md` file, containing the information you'll need, such as color palette and fonts.
  - Technical Challenges:
    - Implement a state management system (e.g., Redux, React Context) for managing form state and error messages.
    - Separate business logic from UI components using appropriate React patterns (e.g., container components, presentational components).
    - Implement form validation using a library or custom logic, and ensure that validation is done both on form submission and as the user types.
    - Utilize Tailwind CSS for styling the components and layout of the signup form.

### Evaluation Criteria

-   Show us your work through your commit history
-   Completeness: did you complete the features?
-   Correctness: does the functionality act in sensible, thought-out ways?
-   Maintainability: is it written in a clean, maintainable way?
-   Testing: is the system adequately tested?

- **TypeScript** Best Practices
  - Proper use of TypeScript types and interfaces.
  - Type safety and error handling.

- **React** Best Practices
    - Component structure and separation of concerns.
    - Effective use of React hooks and lifecycle methods.

- **State Management** 
    - Effective use of state management tools (e.g., Redux, React Context).
    - Proper handling of form state and error messages.

- **UI/UX with Tailwind CSS**
    - Effective use of Tailwind CSS classes for styling.
    - Adherence to the provided design, including responsiveness.
    - Smooth transitions and hover states.

- **Code Quality and Maintainability**
    - Clean and well-organized code.
    - Proper commenting and documentation.
    - Smooth transitions and hover states.


### Deliverables

Make sure to include all source code in the repository. 

### Submission

Please organize, design, test and document your code as if it were going into production - then push your changes to the master branch. After you have pushed your code, you may submit the assignment via a link to your repo/hosted site etc. 

***IMPORTANT! Please don't zip files and email them as attachements. Our spam filter might detect this as spam and will fail to reach our inbox***.

All the best and happy coding,

The Ware Team