# Katas

We use Code Katas which are programming exercises to develop our skills. This could be knowledge of a particular programming language or applying concepts such as Test Driven Development (TDD). Below is a list of Code Katas that we have found useful.

* [Bowling](./bowling)
* [Mars Rover](./mars-rover)
* [Mumbling](./mumbling)
* [Number to LCD](./number-to-lcd)
* [Roman numerals](./roman-numerals)
* [Snake](./snake)
* [Tennis](./tennis)
* [Turnstile](./turnstile)
* [Video Store](./video-store)
* [Word Wrap](./word-wrap)


### Frontend Programming Exercises 

A list of different exercises to improve frontend skills, using an assignment brief to produce a desired objective:

* [SignUp Form](./signup-form)
* [Social Media Dashboard](./social-media-dashboard)


### Backend Programming Exercises

A list of PHP exercises to improve backend api skills. Mainly using the Laravel framework:

* [ToDo App](./todo-app)
* [URL Shortener](./url-shortener)


### Web Developer Exercises

A list of different exercises to improve generic web development skills:

* [Rock Homepage](./rock-homepage)
